// V tej datoteki je opredeljen objekt Klepet, 
// ki nam olajša obvladovanje funkcionalnosti uporabnika


var Klepet = function(socket) {
  this.socket = socket;
};


/**
 * Pošiljanje sporočila od uporabnika na strežnik z uporabo 
 * WebSocket tehnologije po socketu 'sporocilo'
 * 
 * @param kanal ime kanala, na katerega želimo poslati sporočilo
 * @param besedilo sporočilo, ki ga želimo posredovati
 */
Klepet.prototype.posljiSporocilo = function(kanal, besedilo) {
  var sporocilo = {
    kanal: kanal,
    besedilo: besedilo
  };
  this.socket.emit('sporocilo', sporocilo);
};


/**
 * Uporabnik želi spremeniti kanal, kjer se zahteva posreduje na strežnik 
 * z uporabo WebSocket tehnologije po socketu 'pridruzitevZahteva'
 * 
 * @param kanal ime kanala, kateremu se želimo pridružiti
 */
Klepet.prototype.spremeniKanal = function(kanal) {
  this.socket.emit('pridruzitevZahteva', {
    novKanal: kanal
  });
};


/**
 * Procesiranje ukaza, ki ga uporabnik vnese v vnosno polje, kjer je potrebno
 * najprej izluščiti za kateri ukaz gre, nato pa prebrati še parametre ukaza
 * in zahtevati izvajanje ustrezne funkcionalnosti
 * 
 * @param ukaz besedilo, ki ga uporabnik vnese v vnosni obrazec na spletu
 */
Klepet.prototype.procesirajUkaz = function(ukaz) {
  var besede = ukaz.split(' ');
  // Izlušči ukaz iz vnosnega besedila
  ukaz = besede[0].substring(1, besede[0].length).toLowerCase();
  var sporocilo = false;

  switch(ukaz) {
    case 'pridruzitev':
      besede.shift();
      var kanal = besede.join(' ');
      // Sproži spremembo kanala
      this.spremeniKanal(kanal);
      break;
    case 'vzdevek':
      besede.shift();
      var vzdevek = besede.join(' ');
      // Zahtevaj spremembo vzdevka
      this.socket.emit('vzdevekSpremembaZahteva', vzdevek);
      break;
    case 'zasebno':
      besede.shift();
      
      var vzdevki = besede[0].split(',');
      var temp = '';
      for(var i = 0; i < vzdevki[0].length; i++){
        temp += vzdevki[0].charAt(i);
      }
      vzdevki[0] = temp;
      temp = '';
      for(var j = 0; j < vzdevki[vzdevki.length - 1].length - 1; j++){
        temp += vzdevki[vzdevki.length - 1].charAt(j)
      }
      vzdevki[vzdevki.length - 1] = temp;
      
      besede.shift();
      vzdevki.forEach(function(vzdevek){
        var besedilo = besede.join(' ');
        var parametri = besedilo.split('\"');
        if (parametri) {
          this.socket.emit('sporocilo', {vzdevek: vzdevek, besedilo: parametri[1]});
          sporocilo = '(zasebno za ' + vzdevek + '): ' + parametri[1];
        } else {
          sporocilo = 'Neznan ukaz';
        }
      });
      
      break;
    case 'barva':
      besede.shift();
      var Color = besede[0];
      // Nastavimo barvo kanala in sporocil
      document.getElementById("kanal").style.color = Color;
      document.getElementById("sporocila").style.color = Color;
      break;
    default:
      // Vrni napako, če pride do neznanega ukaza
      sporocilo = 'Neznan ukaz.';
      break;
  }

  return sporocilo;
};